from django.test import TestCase
from django.conf import settings
from .models import Status
from .forms import NewStatus
from .views import index
from importlib import import_module
from django.http import HttpRequest
from django.urls import resolve
from django.test import TestCase, Client
from django.utils import timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class CeritaWebTest(TestCase):

    #URL Test
    def test_url_page_found(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_page_not_found(self):
        response = Client().get('/story/')
        self.assertEqual(response.status_code, 404)

    #FUNCTION TEST
    def test_landing_page_using_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #TEMPLATE TEST
    def test_landing_page_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'cerita/index.html')

    #TEXT HTML TEST
    def test_landing_page(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Halo, Apa Kabar?", html_response)

    #MODEL TEST
    def test_create_new_status(self):
        Status.objects.create(
            status_user = 'sedang bikin tugas ppw 6',
            time_status = timezone.now()
        )
        sum_all_status = Status.objects.all().count()
        self.assertEqual(sum_all_status, 1)

    def test_feedback(self):
        obj = Status.objects.create(
            status_user = 'dikit lagi selesai',
            time_status = timezone.now()
        )
        self.assertTrue(isinstance(obj, Status))
        self.assertEqual(obj.__str__(), obj.status_user)

    #FORM TEST
    def test_form_valid(self):
        obj = Status.objects.create(
            status_user = 'insyaallah bisa!',
            time_status = timezone.now()
        )
        data = {
            'status_user' : obj.status_user,
            'time_status' : obj.time_status,
        }
        form = NewStatus(data=data)
        self.assertTrue(form.is_valid())

    def test_form_Invalid(self):
        obj = Status.objects.create(
            status_user = '',
            time_status = 111
        )
        data = {
            'status_user' : obj.status_user,
            'time_status' : obj.time_status,
        }
        form = NewStatus(data=data)
        self.assertFalse(form.is_valid())

class Story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # OPEN THE LINK
        selenium.get('http://127.0.0.1:8000/')
        # FIND ELEMEN
        status = selenium.find_element_by_id('id_status_user')
        submit = selenium.find_element_by_name('status_user')

        status.send_keys('Semangat selaluuu')
        time.sleep(2)
        submit.send_keys(Keys.RETURN)