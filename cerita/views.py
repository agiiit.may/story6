from django.shortcuts import render, redirect
from .models import Status
from .forms import NewStatus

def index(request):
    status = Status.objects.all().order_by('-time_status')
    status_form = NewStatus()

    if request.method == "POST":
        status_form = NewStatus(request.POST)
        if status_form.is_valid():
            status_form.save()

            return redirect('cerita:index')

    context = {
        'judul' : 'Cerita',
        'status' : status,
        'status_form' : status_form
    }

    return render(request, 'cerita/index.html', context)