from django.db import models

class Status(models.Model):
    status_user = models.CharField(max_length=300)
    time_status = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.status_user