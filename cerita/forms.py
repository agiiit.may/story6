from django import forms

from .models import Status

class NewStatus(forms.ModelForm):
    status_user = forms.CharField(max_length=300)

    class Meta:
        model = Status
        fields = [
            'status_user',
        ]

        widgets = {
            'status_user' : forms.Textarea(
                attrs = {
                    'id': "id_status",
                    'class' : 'form-control',
                    'placeholder' : 'dear diary...',
                    'cols' : 50,
                    'row' : 5,
                }
            ),
        }